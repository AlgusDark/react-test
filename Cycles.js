import React from 'react';
import ReactDOM from 'react-dom';

class Cycles extends React.Component {
  constructor(){
    super();
    this.state = { val: 0 };
    this.update = this.update.bind(this);
  }
  update(){
    this.setState({ val: this.state.val + 1 });
  }

  // Life Cycles
  componentWillMount() {
    // console.log('mounting');
    // Ready to go to the DOM
    // Access to states and props
    this.setState({ m: 2 })
  }
  render(){
    console.log('rendering');
    return (
      <button onClick={this.update}>
        {this.state.val * this.state.m}
      </button>
    );
  }
  componentDidMount() {
    // console.log('mounted');
    // Access to the DOM
    console.log(ReactDOM.findDOMNode(this));

    this.inc = setInterval(this.update, 500);
  }
  componentWillUnmount() {
    console.log('bye!');
    clearInterval(this.inc);
  }
}

class ComponentButtons extends React.Component {
  constructor(){
    super();
  }
  getMyComponent(){
    return document.getElementById('myComponent');
  }
  mount(){
    ReactDOM.render(<Cycles/>, this.getMyComponent());
  }
  unmount(){
    ReactDOM.unmountComponentAtNode(this.getMyComponent());
  }
  render(){
    return (
      <div>
        <button onClick={this.mount.bind(this)}>Mount</button>
        <button onClick={this.unmount.bind(this)}>Unmount</button>
        <div id="myComponent"></div>
      </div>
    );
  }
}

export default ComponentButtons;