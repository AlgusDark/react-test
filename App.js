import React from 'react';
import InputText from './InputText';
import Slider from './Slider';
import Button from './Button';
import Cycles from './Cycles';
import UpdateCycle from './UpdateCycle';

// Props are static values

class App extends React.Component {
  render(){ // render() only can return a single node
    return (
      <div>
        <h1>{this.props.title}</h1>
        <InputText/>
        <Slider/>
        <Button>I <Heart/> JS</Button>
        <br/>
        <Cycles/>
      </div>
    );
    
    // return React.createElement('h1', this.props, `${this.props.title}`)
  }
}

App.propTypes = {
  title: React.PropTypes.string
  // total: React.PropTypes.number.isRequired
}

App.defaultProps = {
  title: 'Default Title'
}

const Heart = () => <span>&hearts;</span>

//////////////////////////////////////// 
//    Stateless function Component    //
//////////////////////////////////////// 
//
// const App = () => <h1>Hello</h1>

export default App;