import React from 'react';
import ReactDOM from 'react-dom';

class UpdateCycle extends React.Component {
  constructor(){
    super();
    this.update = this.update.bind(this);
    this.state = {increasing: false}
  }
  
  update(){
    ReactDOM.render(
      <UpdateCycle val={this.props.val+1} />,
      document.getElementById('updateCycle')
    );
  }
  componentWillReceiveProps(nextProps){
    this.setState({increasing: nextProps.val > this.props.val})
  } 
  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.val % 5 === 0;
  }
  render(){
    console.log(this.state.increasing)
    return (
      <button onClick={this.update}>
        {this.props.val}
      </button>)
  }
  componentDidUpdate(prevProps, prevState) {
    console.log('prevProps', prevProps)
  }
}

UpdateCycle.defaultProps = { val: 0 }

ReactDOM.render(
  <UpdateCycle />,
  document.getElementById('updateCycle')
);

export default UpdateCycle;