import React from 'react';

// State are values meant to be managed by the component

class InputText extends React.Component {
  constructor(){
    super(); // context for this
    
    this.state = { 
      title: 'This is a Title State' 
    };

    this.update = this.update.bind(this);
  }

  update(e){
    this.setState({ title: e.target.value });
  }

  render(){ // render() only can return a single node
    return (
      <div>
        <Widget title={this.state.title} update={this.update} />
      </div>
    );
  }
}

const Widget = (props) => {
  return (
    <div>
      <input type="text" onChange={props.update}/>
      <h2>{props.title}</h2>
    </div>
  );
}

export default InputText;